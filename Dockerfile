FROM nixpkgs/nix

COPY nix.conf /etc/nix/nix.conf
COPY . /dgraph
WORKDIR /dgraph
CMD nix shell /dgraph
