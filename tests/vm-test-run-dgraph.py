def set_schema(host, address):
    schema = """type Foo {
    id: ID!
    foo: String
}"""
    return host.execute(f"curl --fail -X POST http://{address}:8080/admin/schema -d '{schema}'")

def test_update_schema(host, address, code, pattern):
    status, stdout = set_schema(host, address)
    if status != code:
        raise Exception(f"Update schema failed with exit code {status}, expected {code}")
    if pattern not in stdout:
        raise Exception(f"Pattern '{pattern}' not found in stdout")

start_all()
dgraph.wait_for_unit("dgraph.zero.service")
dgraph.wait_for_unit("dgraph.alpha.service")

with subtest("check health"):
    dgraph.wait_for_open_port(8080)
    dgraph.succeed("curl http://localhost:6080/state")
    dgraph.succeed("curl http://localhost:8080/health?all")

    client.wait_for_unit("multi-user.target")
    client.succeed("curl http://dgraph:8080/health?all")

with subtest("update schema"):
    test_update_schema(dgraph, "localhost", 0, "Success")

with subtest("client is not in whitelist"):
    test_update_schema(client, "dgraph", 0, "unauthorized ip address")