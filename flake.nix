{
  description = "nix flake for dgraph";
  inputs = {
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs =
    { self
    , nixpkgs
    , flake-utils
    ,
    }:
    flake-utils.lib.eachDefaultSystem
      (
        system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
        in
        rec {
          packages = flake-utils.lib.flattenTree {
            default = pkgs.dgraph;
          };
          formatter = pkgs.nixpkgs-fmt;
          apps.dgraph = flake-utils.lib.mkApp { drv = packages.default; };
        }
      )
    // {
      nixosModules = {
        dgraphAlpha =
          { config
          , lib
          , pkgs
          , ...
          }:
            with lib; let
              cfg = config.dgraph.services.alpha;
              pkg = pkgs.dgraph;
              execAlpha = ''
                ${pkg}/bin/dgraph alpha \
                  --my "${cfg.address}:${toString cfg.port}" \
                  --wal /var/lib/dgraph.alpha \
                  --zero="${cfg.zero}" \
                  --security "token=${cfg.security.token}; whitelist=${cfg.security.whitelist};"
              '';
            in
            {
              options.dgraph.services.alpha = {
                enable = mkEnableOption "Enables dgraph alpha service";
                address = mkOption rec {
                  type = types.str;
                  default = "localhost";
                  example = default;
                  description = "Address the service listens on";
                };
                port = mkOption rec {
                  type = types.port;
                  default = 7080;
                  example = default;
                  description = "Port the service listens on";
                };
                zero = mkOption rec {
                  type = types.str;
                  default = "localhost:5080";
                  example = default;
                  description = "Address dgraph zero listens on";
                };
                security.token = mkOption rec {
                  type = types.str;
                  default = "";
                  example = "afcc062dd1326c36fcaf605d131dd75cb093925f03bec1120defe57d365a7c7f=";
                  description = "If set, all Admin requests to Dgraph will need to have this token. The token can be passed as follows: for HTTP requests, in the X-Dgraph-AuthToken header. For Grpc, in auth-token key in the context.";
                };
                security.whitelist = mkOption rec {
                  type = types.commas;
                  default = "127.0.0.1,::1,localhost";
                  example = "144.142.126.254,127.0.0.1:127.0.0.3,192.168.0.0/16,host.docker.internal";
                  description = "A comma separated list of IP addresses, IP ranges, CIDR blocks, or hostnames you wish to whitelist for performing admin actions";
                };
              };
              config = mkIf cfg.enable {
                systemd.services."dgraph.alpha" = {
                  description = "dgraph.io alpha instance";
                  path = [ pkgs.nodejs ];
                  wantedBy = [ "multi-user.target" ];
                  wants = [ "network.target" ];
                  after = [ "network.target" "dgraph.zero.service" ];
                  requires = [ "dgraph.zero.service" ];
                  serviceConfig = {
                    Restart = "on-failure";
                    ExecStart = execAlpha;
                    LimitNOFILE = "65536";
                    DynamicUser = "yes";
                    WorkingDirectory = "/var/lib/dgraph.alpha";
                    RuntimeDirectory = "dgraph.alpha";
                    RuntimeDirectoryMode = "0755";
                    StateDirectory = "dgraph.alpha";
                    StateDirectoryMode = "0700";
                    CacheDirectory = "dgraph.alpha";
                    CacheDirectoryMode = "0750";
                  };
                };
              };
            };
        dgraphZero =
          { config
          , lib
          , pkgs
          , ...
          }:
            with lib; let
              cfg = config.dgraph.services.zero;
              pkg = pkgs.dgraph;
              execZero = ''
                ${pkg}/bin/dgraph zero \
                  --my "${cfg.address}:${toString cfg.port}" \
                  --wal /var/lib/dgraph.zero \
                  --raft="idx=1"
              '';
            in
            {
              options.dgraph.services.zero = {
                enable = mkEnableOption "Enables dgraph zero service";
                address = mkOption rec {
                  type = types.str;
                  default = "localhost";
                  example = default;
                  description = "Address the service listens on";
                };
                port = mkOption rec {
                  type = types.port;
                  default = 5080;
                  example = default;
                  description = "Port the service listens on";
                };
              };
              config = mkIf cfg.enable {
                systemd.services."dgraph.zero" = {
                  description = "dgraph.io Zero instance";
                  wantedBy = [ "multi-user.target" ];
                  wants = [ "network.target" ];
                  after = [ "network.target" ];
                  requiredBy = [ "dgraph.alpha.service" ];
                  serviceConfig = {
                    Restart = "on-failure";
                    ExecStart = execZero;
                    LimitNOFILE = "65536";
                    DynamicUser = "yes";
                    WorkingDirectory = "/var/lib/dgraph.zero";
                    RuntimeDirectory = "dgraph.zero";
                    RuntimeDirectoryMode = "0755";
                    StateDirectory = "dgraph.zero";
                    StateDirectoryMode = "0700";
                    CacheDirectory = "dgraph.zero";
                    CacheDirectoryMode = "0750";
                  };
                };
              };
            };
      };
      nixosConfigurations.container = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          self.nixosModules.dgraphZero
          self.nixosModules.dgraphAlpha
          ({ pkgs, ... }: {
            boot.isContainer = true;
            networking.firewall.allowedTCPPorts = [ 5080 6080 8080 9080 ];
            dgraph.services.zero.enable = true;
            dgraph.services.zero.address = "0.0.0.0";
            dgraph.services.alpha.enable = true;
            dgraph.services.alpha.address = "0.0.0.0";
            dgraph.services.alpha.security.whitelist = "127.0.0.1,localhost,10.233.1.1";
          })
        ];
      };
      checks.x86_64-linux = {
        build = self.packages.x86_64-linux.default;
        test = with import (nixpkgs + "/nixos/lib/testing-python.nix")
          { system = "x86_64-linux"; };
          makeTest {
            name = "dgraph";
            nodes.dgraph = { ... }: {
              imports = [ self.nixosModules.dgraphZero self.nixosModules.dgraphAlpha ];
              virtualisation.vlans = [ 1 ];
              networking.firewall.allowedTCPPorts = [ 5080 6080 8080 9080 ];
              networking.firewall.allowPing = true;
              dgraph.services.zero.enable = true;
              dgraph.services.alpha.enable = true;
              dgraph.services.alpha.address = "0.0.0.0";
              dgraph.services.alpha.security.whitelist = "localhost,dgraph";
            };
            nodes.client = { ... }: {
              virtualisation.vlans = [ 1 ];
            };
            testScript = builtins.readFile ./tests/vm-test-run-dgraph.py;
          };
      };
    };
}
